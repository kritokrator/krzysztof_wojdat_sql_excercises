/* 19.1 */
delete from klient where id = 17;
/* 19.2 */

delete from wypozyczenie where id_klient = 17;

/* 19.3 */
delete from klient where id not in ( select id_klient from wypozyczenie); 

/*20.1*/

insert into klient (id, imie, nazwisko, ulica, numer, miasto,kod,telefon)
	values (21,'ADAM', 'CICHY', 'KORZENNA',12, 'WARSZAWA', '00-950', '123-454-312');

/*20.2*/

insert into samochod values (19, 'SKODA','OCTAVIA','2012-09-01','SREBRNY',1896,5000);

/*20.3*/

insert into wypozyczenie (id,id_klient,id_samochod,id_pracow_wyp,id_miejsca_wyp,data_wyp,kaucja,cena_jedn) values
	(26,21,19,1,2,'2012-10-28',4000,500);

/*21.1*/

select imie, nazwisko, data_zatr from pracownik 
	where extract(month from data_zatr) = 5 
	order by nazwisko, imie;

/*21.2*/
select imie, nazwisko, current_date - data_zatr as przepracowano from pracownik;

/*21.3*/
select k.imie, k.nazwisko, s.marka, s.typ, current_date - w.data_wyp as dni_od_wypozyczenia from klient k join wypozyczenie w on k.id = w.id_klient join samochod s on s.id = w.id_samochod where w.data_odd is null;
 

