/*16.1*/
select k.imie, k.nazwisko, count(w.id) as ilosc_wyp 
from klient k left join wypozyczenie w on k.id = w.id_klient
 group by k.imie, k.nazwisko order by ilosc_wyp desc;

 /*16.2*/
 select s.id, s.typ, s.marka, count(w.id) as ilosc_wyp
	from samochod s left join wypozyczenie w on s.id = w.id_samochod
	group by s.id, s.typ, s.marka order by ilosc_wyp;

/*16.3*/
select p.imie, p.nazwisko, count(w.id) as ilosc_wyp
	from pracownik p left join wypozyczenie w on p.id = w.id_pracow_wyp
	group by p.imie, p.nazwisko order by ilosc_wyp;

/* 17.1 */
select k.imie, k.nazwisko, count(w.id) as ilosc_wyp
	from klient k join wypozyczenie w on k.id = w.id_klient
	group by k.imie, k.nazwisko having count(w.id) >= 2;

/*17.2*/
select s.marka, s.typ, count(w.id) as ilosc_wyp
	from samochod s join wypozyczenie w on s.id = w.id_samochod
	group by s.marka, s.typ having count(w.id) =2;

/*17.3*/
select p.imie, p.nazwisko, count(w.id) as ilosc_wyp
	from pracownik p left join wypozyczenie w on p.id = w.id_pracow_wyp
	group by p.imie, p.nazwisko having count(w.id) <= 1;

/*18.1*/
update pracownik set dodatek=50 where dodatek is null returning *;

/*18.2*/
update klient set imie = 'Jerzy', nazwisko = 'Nowak' where id = 10 returning *; 

/*18.3*/
update pracownik set pensja = pensja * 1.1 where id in
	(select id from pracownik where 
	pensja < (select avg(pensja) from pracownik))
	returning *;

