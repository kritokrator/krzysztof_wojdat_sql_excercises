1 Projekcja wyników zapytań(SELECT ... FROM ...)
1.1 Wyświetlić zawartość wszystkich kolumn z tabeli pracownik.
1.2 Z tabeli pracownik wyświetlić same imiona pracowników.
1.3 Wyświetlić zawartość kolumn imię, nazwisko i dział z tabeli pracownik.
2 Sortowanie wyników zapytań (ORDER BY)
2.1 Wyświetlić zawartość kolumn imię, nazwisko i pensja z tabeli pracownik. Wynik posortuj
malejąco względem pensji .
2.2 Wyświetl zawartość kolumn nazwisko i imię z tabeli pracownik. Wynik posortuj rosnąco
(leksykograficznie) względem nazwiska i imienia.
2.3 Wyświetlić zawartość kolumn nazwisko, dział, stanowisko z tabeli pracownik. Wynik
posortuj rosnąco względem działu, a dla tych samych nazw działów malejąco względem
stanowiska.
3 Unikatowe wyniki zapytań (DISTINCT)
3.1 Wyświetlić niepowtarzające się wartości kolumny dział z tabeli pracownik.
3.2 Wyświetlić unikatowe wiersze zawierające wartości kolumn dział i stanowisko w tabeli
pracownik.
3.3 Wyświetlić unikatowe wiersze zawierające wartości kolumn dział i stanowisko w tabeli
pracownik. Wynik posortuj rmalejąco względem działu i stanowiska.
4 Selekcja wyników zapytań (WHERE)
4.1 Znajdź pracowników o imieniu Jan. Wyświetl ich imiona i nazwiska.
4.2 Wyświetlić imiona i nazwiska pracowników pracujących na stanowisku sprzedawca.
4.3 Wyświetlić imiona, nazwiska, pensje pracowników, którzy zarabiają powyżej 1500 zł.
Wynik posortuj malejąco względem pensji.
5 Warunki złożone (AND, OR, NOT)
5.1 Z tabeli pracownik wyświetlić imiona, nazwiska, działy, stanowiska tych pracowników,
którzy pracują w dziale obsługi klienta na stanowisku sprzedawca.
5.2 Znaleźć pracowników pracujących w dziale technicznym na stanowisku kierownika lub
mechanika. Wyświetl imie, nazwisko, dzial, stanowisko.
5.3 Znaleźć samochody, które nie są marek fiat i ford.
6 Predykat IN
6.1 Znaleźć samochody marek mercedes, seat i opel.
6.2 Znajdź pracowników o imionach Anna, Marzena, Alicja. Wyświetl ich imiona nazwiska i
daty zatrudnienia.
6.3 Znajdź klientów, którzy nie mieszkają w Warszawie lub we Wrocławiu. Wyświetl ich 
imiona, nazwiska i miasta zamieszkania.
7 Predykat LIKE
7.1 Wyświetlić imiona i nazwiska klientów, których nazwisko zawiera literę K.
7.2 Wyświetlić imiona i nazwiska klientów, dla których nazwisko zaczyna się na D, a kończy
się na SKI.
7.3 Znaleźć imiona i nazwiska klientów, których nazwisko zawiera drugą literę O lub A.
8 Predykat BETWEEN
8.1 Z tabeli samochód wyświetlić wiersze, dla których pojemność silnika jest z przedziału
[1100,1600].
8.2 Znaleźć pracowników, którzy zostali zatrudnieni pomiędzy datami 1997-01-01 a
1997-12-31.
8.3 Znaleźć samochody, dla których przebieg jest pomiędzy 10000 a 20000 km lub pomiędzy
30000 a 40000 km.
9 Wartość NULL
9.1 Znaleźć pracowników, którzy nie mają określonego dodatku do pensji.
9.2 Wyświetlić klientów, którzy posiadają kartę kredytową.
9.3 Dla każdego pracownika wyświetl imię, nazwisko i wysokość dodatku. Wartość NULL z
kolumny dodatek powinna być wyświetlona jako 0. Wskazówka: Użyj funkcji COALESCE.
10 Kolumny wyliczeniowe (COALESCE)
10.1 Wyświetlić imiona, nazwiska pracowników ich pensje i dodatki oraz kolumnę
wyliczeniową do_zapłaty, zawierającą sumę pensji i dodatku. Wskazówka: Wartość NULL
z kolumny dodatek powinna być liczona jako zero.
10.2 Dla każdego pracownika wyświetl imię, nazwisko i wyliczeniową kolumnę nowa_pensja,
która będzie miała o 50% większą wartość niż dotychczasowa pensja.
10.3 Dla każdego pracownika oblicz ile wynosi 1% zarobków (pensja + dodatek).
Wyświetl imię, nazwisko i obliczony 1%. Wyniki posortuj rosnąco względem obliczonego
1%.
11 Złączenia wewnętrzne dwóch tabel
11.1 Wyszukaj samochody, który nie zostały zwrócone. (Data oddania samochodu ma mieć
wartość NULL.) Wyświetl identyfikator, markę i typ samochodu oraz jego datę wypożyczenia i
oddania.
11.2 Wyszukaj klientów, którzy nie zwrócili jeszcze samochodu. . (Data oddania samochodu
ma mieć wartość NULL.) Wyświetl imię i nazwisko klienta oraz identyfikator i datę
wypożyczenia nie zwróconego jeszcze samochodu. Wynik posortuj rosnąco względem
nazwiska i imienia klienta oraz identyfikatorze i dacie wypożyczenia samochodu.
11.3 Dla każdego klienta wyszukaj daty i kwoty wpłaconych kaucji. Wyświetl imię i nazwisko
klienta oraz datę wpłacenia kaucji (ta sama data co data wypożyczenia samochodu) i jej 
wysokość (różną od NULL).
12 Złączenia wewnętrzne większej liczby tabel
12.1 Dla każdego klienta. który choć raz wypożyczył samochód, wyszukaj jakie i kiedy
wypożyczył samochody. Wyświetl imię i nazwisko klienta oraz markę i typ wypożyczonego
samochodu. Wynik posortuj rosnąco po nazwisku i imieniu klienta oraz marce i typie
samochodu.
12.2 Dla każdej filii wypożyczalni samochodów (tabela miejsce) wyszukaj jakie samochody
były wypożyczane. Wyświetl adres filii (ulica i numer) oraz markę i typ wypożyczonego
samochodu. Wyniki posortuj rosnąco względem adresu filii, marki i typu damochodu.
12.3 Dla każdego wypożyczonego samochodu wyszukaj informację jacy klienci go
wypożyczali. Wyświetl identyfikator, markę i typ samochodu oraz imię i nazwisko klienta.
Wyniki posortuj po identyfikatorze samochodu oraz nazwisku i imieniu klienta.
13 Funkcje agregujące bez grupowania
13.1 Znaleźć największą pensję pracownika.
13.2 Znaleźć średnią pensję pracownika.
13.3 Znaleźć najwcześniejszą datę wyprodukowania samochodu .
14 Podzapytania nieskorelowane z użyciem funkcji agregujących bez
grupowania
14.1 Wyświetl imiona, nazwiska i pensje pracowników, którzy posiadają najwyższą pensją.
14.2 Wyświetl pracowników (imiona, nazwiska, pensje), którzy zarabiają powyżej średniej
pensji.
14.3 Wyszukaj samochody (marka, typ, data produkcji), które zostały wyprodukowane
najwcześniej.
15 Podzapytania nieskorelowane z predykatem IN
15.1 Wyświetl wszystkie samochody (marka, typ, data produkcji), które do tej pory nie zostały
wypożyczone.
15.2 Wyświetl klientów (imię i nazwisko), którzy do tej pory nie wypożyczyli żadnego
samochodu. Wynik posortuj rosnąco względem nazwiska i imienia klienta.
15.3 Znaleźć pracowników (imię i nazwisko), którzy do tej pory nie wypożyczyli żadnego
samochodu klientowi.
16 Funkcje agregujące z grupowaniem (GROUP BY)
16.1 Dla każdego klienta wypisz imię, nazwisko i łączną ilość wypożyczeń samochodów (nie
zapomnij o zerowej liczbie wypożyczeń). Wynik posortuj malejąco względem ilości
wypożyczeń.
16.2 Dla każdego samochodu (identyfikator, marka, typ) oblicz ilość wypożyczeń. Wynik
posortuj rosnąco względem ilości wypożyczeń.
16.3 Dla każdego pracownika oblicz ile wypożyczył samochodów klientom. Wyświetl imię i
nazwisko pracownika oraz ilość wypożyczeć. Wynik posortuj malejąco po ilości wypożyczeń.
17 Warunki na funkcje agregujące (HAVING)
17.1 Znajdź klientów, którzy co najmniej 2 razy wypożyczyli samochód. Wypisz dla tych
klientów imię, nazwisko i ilość wypożyczeń. Wynik posortuj rosnąco względem imienia i
nazwiska.
17.2 Znajdź samochody, które były wypożyczone dokładnie dwa razy. Wyświetl identyfikator
samochodu, markę, typ i ilość wypożyczeń. Wynik posortuj rosnąco względem marki i typu
samochodu.
17.3 Znajdź pracowników, którzy klientom wypożyczyli co najwyżej jeden samochód.
Wyświetl imiona i nazwiska pracowników razem z ilością wypożyczeń samochodów. Wynik
posortuj rosnąco względem ilości wypożyczeń, nazwiska i imienia pracownika.
18 Modyfikacja danych w bazie danych (UPDATE)
18.1 Pracownikom, którzy nie mają określonej wysokości dodatku nadaj dodatek w
wysokości 50 zł.
18.2 Klientowi o identyfikatorze równym 10 zmień imię i nazwisko na Jerzy Nowak.
18.3 Podwyższyć o 10% pensję pracownikom, którzy zarabiają poniżej średniej.
19 Usuwanie danych z bazy danych (DELETE)
19.1 Usunąć klienta o identyfikatorze równym 17.
19.2 Usunąć wszystkie informacje o wypożyczeniach dla klienta o identyfikatorze równym 17.
19.3 Usunąć klientów, którzy nie wypożyczyli żadnego samochodu.
20 Dodawanie danych do bazy danych (INSERT)
20.1 Dodaj do bazy danych klienta o identyfikatorze równym 21: Adam Cichy zamieszkały ul.
Korzenna 12, 00-950 Warszawa, tel. 123-454-321.
20.2 Dodaj do bazy danych nowy samochód o identyfikatorze równym 19: srebrna skoda
octavia o pojemności silnika 1896 cm3 wyprodukowana 1 września 2012 r. i o przebiegu 5 000
km.
20.3 Dopisz do bazy danych informację o wypożyczeniu samochodu o identyfikatorze 19
przez klienta o identyfikatorze 21 w dniu 28 października 2012 r. przez pracownika o
identyfikatorze 1 w miejscu o identyfikatorze 2. Została pobrana kaucja 4000 zł, a cena za
dzień wypożyczenia wynosi 500 zł.
21 Wybrane funkcje daty i czasu (DAY, MONTH, YEAR, GETDATE, DATEDIFF)
21.1 Wyszukaj pracowników zatrudnionych w miesiącu maju. Wyświetl ich imiona, nazwiska i
datę zatrudnienia. Wynik posortuj rosnąco względem nazwiska i imienia.
21.2 Dla każdego pracownika (imię i nazwisko) wypisz ile już pracuje dni. Wynik posortuj
malejąco według ilości przepracowanych dni.
21.3 Wyszukaj klientów (imiona i nazwiska), którzy nie oddali jeszcze samochodu. Wyświetl
jaki wypożyczyli samochód (marka i typ) i ile dni już go trzymają. Wynik posortuj malejąco
względem ilości dni.
22 Wybrane funkcje operujące na napisach (LEFT, RIGHT, LEN,
UPPER, LOWER, STUFF)
22.1 Wyświetl imię, nazwisko i inicjały każdego klienta. Wynik posortuj względem inicjałów,
nazwiska i imienia klienta.
22.2 Wyświetl imiona i nazwiska klientów w taki sposób, aby pierwsza litera imienia i nazwiska
była wielka, a pozostałe małe.
22.3 Wyświetl imiona, nazwiska i numery kart kredytowych klientów. Każda z ostatnich
sześciu cyfr wyświetlanego numeru karty kredytowej klienta powinna być zastąpiona znakiem
x 

