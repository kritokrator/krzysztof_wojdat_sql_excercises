﻿/* 1.2 */
select imie from pracownik;
/* 1.3 */
select imie,nazwisko,dzial from pracownik;
/* 2.1 */
select imie, nazwisko, dzial,pensja from pracownik order by pensja desc;
/* 2.2 */
select imie, nazwisko from pracownik order by nazwisko, imie asc;
/* 2.3 */
select nazwisko, dzial, stanowisko from pracownik order by dzial asc, stanowisko desc;
/* 3.1 */
select distinct dzial from pracownik;
/* 3.2 */
select distinct dzial, stanowisko from pracownik order by dzial, stanowisko;
/* 4.1 */
select imie, nazwisko from pracownik where imie = 'JAN';
/* 4.2 */
select imie, nazwisko,stanowisko from pracownik where stanowisko = 'SPRZEDAWCA';
/* 4.3 */
select imie, nazwisko, pensja from pracownik where pensja > 1500 order by pensja desc;
/* 5.1 */
select imie, nazwisko, dzial, stanowisko from pracownik where dzial = 'OBSLUGA KLIENTA' and stanowisko = 'SPRZEDAWCA';

/* 5.2 */
select imie, nazwisko, dzial, stanowisko from pracownik
 where dzial = 'TECHNICZNY' and ( stanowisko = 'KIEROWNIK' or stanowisko = 'MECHANIK');
/* 5.3 */
 select * from samochod where 
	marka not in ('FORD', 'FIAT');
/* 6.1 */
 select * from samochod where 
	marka  in ('SEAT', 'OPEL','MERCEDES');
/*6.2*/
select imie,nazwisko,data_zatr from pracownik where imie in ('ANNA', 'MARZENA','ALICJA');
/*6.3*/
select imie, nazwisko, miasto from klient where miasto not in ('WROCLAW','WARSZAWA');
/*7.1*/
select imie, nazwisko from klient where nazwisko like 'K%';
/*7.2*/
select imie, nazwisko from klient where nazwisko like 'D%SKI';
/*7.3*/
select imie, nazwisko from klient where nazwisko like '_O%' or nazwisko like '_A%';
select imie, nazwisko from klient where nazwisko similar to '_(O|A)%';

/* 8 */
select * from samochod where poj_silnika between 1100 and 1600;
select * from pracownik where data_zatr between '1997-01-01' and '1997-12-31';
select * from samochod where przebieg between 10000 and 20000 or przebieg between 30000 and 40000;

/*9*/
select * from pracownik where dodatek is null;
select * from klient where nr_karty_kredyt is not null;
select imie, nazwisko, coalesce(dodatek,0) as dodatek from pracownik;
/*10*/
select imie, nazwisko, pensja, dodatek, coalesce(pensja,0) + coalesce(dodatek,0) as do_zaplaty from pracownik;
select imie, nazwisko, coalesce(pensja,0) * 1.5 as nowa_pensja from pracownik;
select imie, nazwisko, coalesce(pensja,0) * 0.01 as procent from pracownik order by procent desc;

/* 11 */
select s.id, s.marka, s.typ, w.data_wyp, w.data_odd from samochod s JOIN wypozyczenie w ON w.id_samochod = s.id
	where w.data_odd is null;

select k.*, w.* from klient k join wypozyczenie w on w.id_klient = k.id
	where w.data_odd is null;

select k.imie, k.nazwisko, w.data_wyp, coalesce(w.kaucja,0) as kaucja from klient k join wypozyczenie w on w.id_klient = k.id;
/* 12 */

select k.imie, k.nazwisko, s.marka, s.typ from
 klient k join wypozyczenie w on w.id_klient = k.id join samochod s on w.id_samochod = s.id
  order by k.nazwisko, k.imie, s.marka, s.typ;

  /*12.2*/

  select m.ulica ,m.numer,m.miasto, s.marka,s.typ from
   wypozyczenie w join miejsce m on w.id_miejsca_wyp = m.id join samochod s on w.id_samochod = s.id
   order by m.ulica, m.numer, s.marka, s.typ;

  /*12.3*/
  select s.id, s.typ, s.marka, k.nazwisko, k.imie from
  wypozyczenie w join samochod s on w.id_samochod = s.id join klient k on w.id_klient = k.id
  order by s.id, s.marka, s.typ, k.nazwisko;

/* 13. 1 */
  select max(pensja) from pracownik;

/* 13. 2 */
  select avg(pensja) from pracownik;
/* 13.3 */
  select min(data_prod) from samochod;

/* 14.1 */
select imie, nazwisko, pensja from pracownik where pensja = ( select max(pensja) from pracownik );
/* 14.2 */
select imie, nazwisko, pensja from pracownik where pensja > ( select avg(pensja) from pracownik );
/* 14.3 */
select * from samochod;

/*15.1*/
select typ, marka, data_prod from samochod where id not in (select id_samochod from wypozyczenie);
/* 15.2 */
select nazwisko, imie from klient where id not in ( select id_klient from wypozyczenie) order by nazwisko;
/*15.3 */
select imie, nazwisko from pracownik where id not in (select id_pracow_wyp from wypozyczenie);

/*16.1*/
select k.imie, k.nazwisko, count(w.id) as ilosc_wyp 
from klient k left join wypozyczenie w on k.id = w.id_klient
 group by k.imie, k.nazwisko order by ilosc_wyp desc;

 /*16.2*/
 select s.id, s.typ, s.marka, count(w.id) as ilosc_wyp
	from samochod s left join wypozyczenie w on s.id = w.id_samochod
	group by s.id, s.typ, s.marka order by ilosc_wyp;

/*16.3*/
select p.imie, p.nazwisko, count(w.id) as ilosc_wyp
	from pracownik p left join wypozyczenie w on p.id = w.id_pracow_wyp
	group by p.imie, p.nazwisko order by ilosc_wyp;

/* 17.1 */
select k.imie, k.nazwisko, count(w.id) as ilosc_wyp
	from klient k join wypozyczenie w on k.id = w.id_klient
	group by k.imie, k.nazwisko having count(w.id) >= 2;

/*17.2*/
select s.marka, s.typ, count(w.id) as ilosc_wyp
	from samochod s join wypozyczenie w on s.id = w.id_samochod
	group by s.marka, s.typ having count(w.id) =2;

/*17.3*/
select p.imie, p.nazwisko, count(w.id) as ilosc_wyp
	from pracownik p left join wypozyczenie w on p.id = w.id_pracow_wyp
	group by p.imie, p.nazwisko having count(w.id) <= 1;

/*18.1*/
/*
update pracownik set dodatek=50 where dodatek is null returning *;
*/
/*18.2*/
/*
 update klient set imie = 'Jerzy', nazwisko = 'Nowak' where id = 10 returning *; 
*/
/*18.3*/
/*
update pracownik set pensja = pensja * 1.1 where id in
	(select id from pracownik where 
	pensja < (select avg(pensja) from pracownik))
	returning *;
*/

/*
19.1
delete from klient where id = 17;
*/
/* 19.2 */
/*
delete from wypozyczenie where id_klient = 17;
*/
/* 19.3 */
/* delete from klient where id not in ( select id_klient from wypozyczenie); */

/*20.1*/
/*
insert into klient (id, imie, nazwisko, ulica, numer, miasto,kod,telefon)
	values (21,'ADAM', 'CICHY', 'KORZENNA',12, 'WARSZAWA', '00-950', '123-454-312');
*/
/*20.2*/
/*
insert into samochod values (19, 'SKODA','OCTAVIA','2012-09-01','SREBRNY',1896,5000);
*/
/*20.3*/
/*
insert into wypozyczenie (id,id_klient,id_samochod,id_pracow_wyp,id_miejsca_wyp,data_wyp,kaucja,cena_jedn) values
	(26,21,19,1,2,'2012-10-28',4000,500);
*/
/*21.1*/

select imie, nazwisko, data_zatr from pracownik 
	where extract(month from data_zatr) = 5 
	order by nazwisko, imie;

/*21.2*/
select imie, nazwisko, current_date - data_zatr as przepracowano from pracownik;

/*21.3*/
select k.imie, k.nazwisko, s.marka, s.typ, current_date - w.data_wyp as dni_od_wypozyczenia from klient k join wypozyczenie w on k.id = w.id_klient join samochod s on s.id = w.id_samochod where w.data_odd is null;
 

/*22.1*/
select imie, nazwisko, substring(imie from 1 for 1) as inicjal_imie, substring(nazwisko from 1 for 1) as inicjal_nazwisko
	from klient order by inicjal_nazwisko, inicjal_imie;

/*22.2*/
select upper(substring(imie from 1 for 1)) || lower(substring(imie from 2 for char_length(imie))),
	upper(substring(nazwisko from 1 for 1)) || lower(substring(nazwisko from 2 for char_length(nazwisko) )) from klient;

/*22.3*/
select imie, nazwisko, overlay(nr_karty_kredyt placing 'XXXXXX' from char_length(nr_karty_kredyt) -5 for 6) from klient;
