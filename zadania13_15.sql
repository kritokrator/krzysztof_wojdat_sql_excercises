
/* 13. 1 */
  select max(pensja) from pracownik;

/* 13. 2 */
  select avg(pensja) from pracownik;
/* 13.3 */
  select min(data_prod) from samochod;

/* 14.1 */
select imie, nazwisko, pensja from pracownik where pensja = ( select max(pensja) from pracownik );
/* 14.2 */
select imie, nazwisko, pensja from pracownik where pensja > ( select avg(pensja) from pracownik );
/* 14.3 */
select * from samochod;

/*15.1*/
select typ, marka, data_prod from samochod where id not in (select id_samochod from wypozyczenie);
/* 15.2 */
select nazwisko, imie from klient where id not in ( select id_klient from wypozyczenie) order by nazwisko;
/*15.3 */
select imie, nazwisko from pracownik where id not in (select id_pracow_wyp from wypozyczenie);
  

