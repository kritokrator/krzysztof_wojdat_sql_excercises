/*22.1*/
select imie, nazwisko, substring(imie from 1 for 1) as inicjal_imie, substring(nazwisko from 1 for 1) as inicjal_nazwisko
	from klient order by inicjal_nazwisko, inicjal_imie;

/*22.2*/
select upper(substring(imie from 1 for 1)) || lower(substring(imie from 2 for char_length(imie))),
	upper(substring(nazwisko from 1 for 1)) || lower(substring(nazwisko from 2 for char_length(nazwisko) )) from klient;

/*22.3*/
select imie, nazwisko, overlay(nr_karty_kredyt placing 'XXXXXX' from char_length(nr_karty_kredyt) -5 for 6) from klient;
